﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Microsoft.Office.Interop.Excel;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;


namespace SecurityBase
{
    [Serializable]
    class DataStorage : ISerializable
    {
        public List<Record> records = new List<Record>();
        public List<ChangesRecord> recordChanges = new List<ChangesRecord>();
        const string downloadSource = "https://bdu.fstec.ru/documents/files/thrlist.xlsx";
        const string nameExcel = "thrlist.xlsx";
        const string nameDst = "thrlist.dst";
        public string PathExcel { get; set; }
        public string PathDst { get; set; }
        public DataStorage()
        {
            PathExcel = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, nameExcel);
            PathDst = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, nameDst);

        }
        public bool DownloadExcel()
        {
            WebClient Client = new WebClient();
            try
            {
                Client.DownloadFile(downloadSource, PathExcel);
            }
            catch (WebException webEx)
            {
                return false;
            }
            return true;
        }
        public void PopulateFromExcel()
        {
            Application ObjExcel = new Application();
            Workbook ObjWorkBook = ObjExcel.Workbooks.Open(PathExcel, 0, true, 5, "", "", false, XlPlatform.xlWindows, "", true, false, 0, true, false, false);
            Worksheet objWorkSheet= (Worksheet)ObjWorkBook.Sheets[1];
            int lastUsedRow = objWorkSheet.Cells.SpecialCells(XlCellType.xlCellTypeLastCell, Type.Missing).Row;
            for (int i=3;i<= lastUsedRow;i++)
            {
                Record record = new Record();           
                record.ThreatID = (int)objWorkSheet.Cells[i, 1].Value;
                record.ThreatName= objWorkSheet.Cells[i, 2].Value.ToString();
                record.ThreatDescription = objWorkSheet.Cells[i, 3].Value.ToString();
                record.ThreatSource = objWorkSheet.Cells[i, 4].Value.ToString();
                record.ThreatObject = objWorkSheet.Cells[i, 5].Value.ToString();
                record.ConfidentialityViolation = Convert.ToBoolean(objWorkSheet.Cells[i, 6].Value);
                record.IntegrityViolation = Convert.ToBoolean(objWorkSheet.Cells[i, 7].Value);
                record.AccessibilityViolation = Convert.ToBoolean(objWorkSheet.Cells[i, 8].Value);
                records.Add(record);
            }
        }
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("records", records);
        }
        public DataStorage(SerializationInfo info, StreamingContext context)
        {
            records = (List<Record>)info.GetValue("records", typeof(List<Record>));
            
        }
        public void SerializeRecords()
        {
            IFormatter formatter = new BinaryFormatter();
            FileStream s = new FileStream(PathDst, FileMode.Create);
            formatter.Serialize(s, records);
            s.Close();
        }
        public void DeserializeRecords()
        {
            IFormatter formatter = new BinaryFormatter();
            FileStream s = new FileStream(PathDst, FileMode.Open);
            records= (List<Record>)formatter.Deserialize(s);
        }
        public void MakeUpdatedRecordList(List<Record> updatedRecords)
        {
            //создаем список удаленных записей
            recordChanges = new List<ChangesRecord>();
            List<Record> inRecordsButNotInUpdatedRecords = (List<Record>)records.Where(p => !updatedRecords.Any(p2 => p2.ThreatID == p.ThreatID)).ToList();
            List<ViewableRecord> inRecordsButNotInUpdatedRecordsViewable = MakeViewableList(inRecordsButNotInUpdatedRecords);
            List<ChangesRecord> deletedRecords = (List<ChangesRecord>)inRecordsButNotInUpdatedRecordsViewable.Select
                (x => new ChangesRecord
                {
                    ChangeStatus = "Запись удалена.",
                    ThreatID = x.ThreatID,
                    ThreatName = x.ThreatName,
                    ThreatDescription = x.ThreatDescription,
                    ThreatSource = x.ThreatSource,
                    ThreatObject = x.ThreatObject,
                    ConfidentialityViolation = x.ConfidentialityViolation,
                    IntegrityViolation = x.IntegrityViolation,
                    AccessibilityViolation = x.AccessibilityViolation
                }).ToList();
            // создаем список добавленных записей
            List<Record> inUpdatedRecordsButNotInRecords = (List<Record>)updatedRecords.Where(p => !records.Any(p2 => p2.ThreatID == p.ThreatID)).ToList();
            List<ViewableRecord> inUpdatedRecordsButNotInRecordsViewable = MakeViewableList(inUpdatedRecordsButNotInRecords);
            List<ChangesRecord> addedRecords = (List<ChangesRecord>)inUpdatedRecordsButNotInRecordsViewable.Select
                (x => new ChangesRecord
                {
                    ChangeStatus = "Запись добавлена.",
                    ThreatID = x.ThreatID,
                    ThreatName = x.ThreatName,
                    ThreatDescription = x.ThreatDescription,
                    ThreatSource = x.ThreatSource,
                    ThreatObject = x.ThreatObject,
                    ConfidentialityViolation = x.ConfidentialityViolation,
                    IntegrityViolation = x.IntegrityViolation,
                    AccessibilityViolation = x.AccessibilityViolation
                }).ToList();

            // создаем список измененных записей    
            List<Record> inRecordsAndUpdatedRecordsNew = (List<Record>)updatedRecords.Where(p => records.Any(p2 => p2.ThreatID == p.ThreatID && (p2.ThreatSource!=p.ThreatSource||p2.ThreatObject!=p.ThreatObject||p2.ThreatName!=p.ThreatName||p2.ThreatDescription!=p.ThreatDescription||p2.IntegrityViolation!=p.IntegrityViolation||p2.ConfidentialityViolation!=p.ConfidentialityViolation||p2.AccessibilityViolation!=p.AccessibilityViolation))).ToList();
            List<Record> inRecordsAndUpdatedRecordsOld = (List<Record>)records.Where(p => inRecordsAndUpdatedRecordsNew.Any(p2 => p2.ThreatID== p.ThreatID)).ToList();
            List<ViewableRecord> inRecordsAndUpdatedRecordsNewViewable = MakeViewableList(inRecordsAndUpdatedRecordsNew);
            List<ViewableRecord> inRecordsAndUpdatedRecordsOldViewable = MakeViewableList(inRecordsAndUpdatedRecordsOld);

            List<ChangesRecord> changedRecords = new List<ChangesRecord>();

            //foreach (ViewableRecord sameIdRecord in inRecordsAndUpdatedRecordsNewViewable)
            for (int i=0; i< inRecordsAndUpdatedRecordsNewViewable.Count; i++)
            {
                ViewableRecord sameIdRecord = inRecordsAndUpdatedRecordsNewViewable[i];
                changedRecords.Add(new ChangesRecord
                {
                    ChangeStatus = "Запись изменена: новое значение",
                    ThreatID = sameIdRecord.ThreatID,
                    ThreatName = sameIdRecord.ThreatName,
                    ThreatDescription = sameIdRecord.ThreatDescription,
                    ThreatSource = sameIdRecord.ThreatSource,
                    ThreatObject = sameIdRecord.ThreatObject,
                    AccessibilityViolation = sameIdRecord.AccessibilityViolation,
                    IntegrityViolation = sameIdRecord.IntegrityViolation,
                    ConfidentialityViolation = sameIdRecord.ConfidentialityViolation
                });

                ViewableRecord sameIdRecordOld = inRecordsAndUpdatedRecordsOldViewable[i];
                changedRecords.Add(new ChangesRecord
                {
                    ChangeStatus = "Запись изменена: предыдущее значение",
                    ThreatID = sameIdRecordOld.ThreatID,
                    ThreatName = sameIdRecordOld.ThreatName,
                    ThreatDescription = sameIdRecordOld.ThreatDescription,
                    ThreatSource = sameIdRecordOld.ThreatSource,
                    ThreatObject = sameIdRecordOld.ThreatObject,
                    AccessibilityViolation = sameIdRecordOld.AccessibilityViolation,
                    IntegrityViolation = sameIdRecordOld.IntegrityViolation,
                    ConfidentialityViolation = sameIdRecordOld.ConfidentialityViolation
                });
                //changedRecords.Add("Запись изменена", "Предыдущее значение",  );

            }

            recordChanges = deletedRecords.Concat(addedRecords)
                                    .Concat(changedRecords)
                                    .ToList();
        }

        public List<ViewableRecord> MakeViewableList(List<Record> records)
        {
            List<ViewableRecord> viewableRecords = (List<ViewableRecord>)records.Select(x => new ViewableRecord { ThreatID = "УБИ." + x.ThreatID.ToString("D3"), ThreatName = x.ThreatName, ThreatDescription=x.ThreatDescription, ThreatSource=x.ThreatSource, ThreatObject=x.ThreatObject, ConfidentialityViolation=x.ConfidentialityViolation.ToLocalizedString(), IntegrityViolation=x.IntegrityViolation.ToLocalizedString(), AccessibilityViolation=x.AccessibilityViolation.ToLocalizedString() }).ToList();
            return viewableRecords;
        }

    }
}
