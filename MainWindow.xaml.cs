﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;


namespace SecurityBase
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly PagingCollectionView _cview;
        DataStorage dst;
        DataStorage dstNew;
        bool ifUpdateAvailable;
        bool ifUpdateSaved;
        bool ifNewlyCreated;
        public MainWindow()
        {
            InitializeComponent();
            ifUpdateAvailable = false;
            ifUpdateSaved = false;
            ifNewlyCreated = false;
            dst = new DataStorage();
            if (!File.Exists(dst.PathDst))
            {
                if (MessageBox.Show("Внимание! Локальная база данных не найдена в рабочем каталоге программы. \nЗагрузить базу угроз из сети Интернет (это может занять несколько минут)?", "Локальная база данных не найдена", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
                {
                    Environment.Exit(0);
                }
                else
                {
                    if (!dst.DownloadExcel())
                    {
                        MessageBox.Show("Не удалось загрузить базу угроз из сети Интернет. Проверьте состояние соединения и повторите вход.", "Не удалось загрузить базу угроз", MessageBoxButton.OK, MessageBoxImage.Error);
                        Environment.Exit(0);
                    }
                    this.Cursor = Cursors.Wait;
                    dst.PopulateFromExcel();
                    saveBtn.IsEnabled = true;
                    ifNewlyCreated = true;
                }
            }
            else
            {
                dst.DeserializeRecords();
            }


            this._cview = new PagingCollectionView(dst.MakeViewableList(dst.records), 25);
            this.DataContext = this._cview;
            this.Cursor = Cursors.Arrow;
            msg.Text = "Добро пожаловать в базу данных угроз безопасности!";
        }

        private void OnNextClicked(object sender, RoutedEventArgs e)
        {
            this._cview.MoveToNextPage();
        }

        private void OnPreviousClicked(object sender, RoutedEventArgs e)
        {
            this._cview.MoveToPreviousPage();
        }

        private void OnSaveClicked(object sender, RoutedEventArgs e)
        {
            if (ifUpdateAvailable && !ifNewlyCreated)
            {
                this.dstNew.SerializeRecords();
                ifUpdateSaved = true;
                MessageBox.Show("Обновленная база данных угроз успешно сохранена в рабочем каталоге.", "Сохранение выполнено", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else if (ifNewlyCreated)
            {
                this.dst.SerializeRecords();
                ifNewlyCreated = false;
                MessageBox.Show("Созданная база данных угроз успешно сохранена в рабочем каталоге.", "Сохранение выполнено", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else 
            {
                MessageBox.Show("Обновленные записи отсутствуют, сохранение не будет выполнено.", "Сохранение отменено", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void OnViewClicked(object sender, RoutedEventArgs e)
        {
            if (col2.Header == "Статус записи")
            {
                col2.Binding = new Binding("ThreatName");
                col2.Header = "Наименование угрозы";
                PagingCollectionView _cviewUpdated = new PagingCollectionView(dst.records, 25);
                this.DataContext = _cviewUpdated;
            }
            else
            {
                col2.Binding = new Binding("ChangeStatus");
                col2.Header = "Статус записи";
                PagingCollectionView _cviewUpdated = new PagingCollectionView(dst.recordChanges, 25);
                this.DataContext = _cviewUpdated;
            }
        }

        private void OnExitClicked(object sender, RoutedEventArgs e)
        {
            if ((ifUpdateAvailable && !ifUpdateSaved)||ifNewlyCreated)
            {
                if (MessageBox.Show("Внимание! Обновленная база данных не была сохранена. \nВы действительно хотите выйти?", "База данных не была сохранена", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    Environment.Exit(0);
                    
                }
            }
            else
            {
                Environment.Exit(0);
            }
        }

        private void OnUpdateClicked(object sender, RoutedEventArgs e)
        {
            msg.Text = "Идет соединение с удаленной базой угроз безопасности, ждите...";
            this.Cursor = Cursors.Wait;
            dstNew = new DataStorage();
            if (!dst.DownloadExcel())
            {
                MessageBox.Show("Не удалось загрузить базу угроз из сети Интернет. Проверьте состояние соединения и повторите попытку.", "Не удалось загрузить базу угроз", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            msg.Text = "Идет обработка удаленной базы угроз, ждите...";
            MessageBox.Show("Внимание! Обновление базы может занять несколько минут. Пожалуйста, дождитесь завершения процесса.", "Начало обновления", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            dstNew.PopulateFromExcel();
            dst.MakeUpdatedRecordList(dstNew.records);
            msg.Text = "Обработка удаленной базы угроз успешно завершена!";
            this.Cursor = Cursors.Arrow;
            if (dst.recordChanges.Count == 0)
            {
                MessageBox.Show("Удаленная база угроз не содержит обновленных записей.", "Обновление завершено", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            else
            {
                ifUpdateAvailable = true;
                ifNewlyCreated = false;
                col2.Binding = new Binding("ChangeStatus");
                col2.Header = "Статус записи";
                PagingCollectionView _cviewUpdated = new PagingCollectionView(dst.recordChanges, 25);
                this.DataContext = _cviewUpdated;
                viewBtn.IsEnabled = true;
                saveBtn.IsEnabled = true;
            }
        }

    }
        
}
