﻿using System;


namespace SecurityBase
{
   [Serializable]
    class Record
    {
        public int ThreatID { get; set; }
        public string ThreatName { get; set; }
        public string ThreatDescription { get; set; }
        public string ThreatSource { get; set; }
        public string ThreatObject { get; set; }
        public bool ConfidentialityViolation { get; set; }
        public bool IntegrityViolation { get; set; }
        public bool AccessibilityViolation { get; set; }

    }

    class ViewableRecord
    {
        public string ThreatID { get; set; }
        public string ThreatName { get; set; }
        public string ThreatDescription { get; set; }
        public string ThreatSource { get; set; }
        public string ThreatObject { get; set; }
        public string ConfidentialityViolation { get; set; }
        public string IntegrityViolation { get; set; }
        public string AccessibilityViolation { get; set; }

    }

    class ChangesRecord : ViewableRecord
    {
        public string ChangeStatus { get; set; }

    }

    static class Extensions
    {
        public static string ToLocalizedString(this bool b)
        {
            return b ? "да" : "нет";
        }
    }
}
